from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, ForeignKey, DateTime
from sqlalchemy_serializer import SerializerMixin
import sqlalchemy

Base = declarative_base()


class DivisionStandings(Base, SerializerMixin):

    __tablename__ = "division_standings"
    metadata = sqlalchemy.MetaData()

    id = Column(Integer, primary_key=True, autoincrement=True)
    team = Column(String)
    games_played = Column(Integer)
    wins = Column(Integer)
    losses = Column(Integer)
    overtime_losses = Column(Integer)
    points = Column(Integer)
    points_pct = Column(Float)
    goals_for = Column(Integer)
    goals_against = Column(Integer)
    simple_rating_system = Column(Float)
    strength_of_schedule = Column(Float)
    points_pct_no_ot = Column(Float)
    wins_in_regulation = Column(Integer)
    regulation_time_record = Column(String)
    point_pct_in_reg_time = Column(Float)
    division = Column(String)
    year = Column(Integer)

    def __repr__(self):
        return f"<DivisionStandings {self.team}>"


class LeagueStandings(Base, SerializerMixin):

    __tablename__ = "league_standings"

    id = Column(Integer, primary_key=True, autoincrement=True)
    metadata = sqlalchemy.MetaData()
    rank = Column(Integer)
    team = Column(String, primary_key=True)
    avg_age = Column(Float)
    games_played = Column(Integer)
    wins = Column(Integer)
    losses = Column(Integer)
    overtime_losses = Column(Integer)
    points = Column(Integer)
    points_pct = Column(Float)
    goals_for = Column(Integer)
    goals_against = Column(Integer)
    shootout_wins = Column(Integer)
    shootout_losses = Column(Integer)
    simple_rating_system = Column(Float)
    strength_of_schedule = Column(Float)
    total_goals_per_game = Column(Integer)
    even_strength_goals_for = Column(Integer)
    even_strength_goals_against = Column(Integer)
    power_play_goals_for = Column(Integer)
    power_play_opportunities_for = Column(Integer)
    power_play_pct = Column(Float)
    power_play_goals_against = Column(Integer)
    power_play_opportunities_against = Column(Integer)
    penalty_kill_pct = Column(Float)
    short_handed_goals_for = Column(Integer)
    short_handed_goals_against = Column(Integer)
    penalty_minutes_per_game = Column(Float)
    opponent_penalty_minutes_per_game = Column(Float)
    shots = Column(Integer)
    shooting_pct = Column(Float)
    shots_against = Column(Integer)
    save_pct = Column(Float)
    shutouts = Column(Integer)
    year = Column(Integer)

    def __repr__(self):
        return f"<LeagueStandings {self.team}>"


class LastFiveGames(Base, SerializerMixin):

    __tablename__ = "player_last_five_games"
    metadata = sqlalchemy.MetaData()

    player_id = Column(Integer, nullable=False, primary_key=True)
    player_name = Column(String)
    date = Column(String)
    team = Column(String)
    opponent = Column(String)
    outcome = Column(String)
    goals = Column(Integer, nullable=False)
    assists = Column(Integer, nullable=False)
    points = Column(Integer, nullable=False)
    plus_minus = Column(Integer, nullable=False)
    penalty_minutes = Column(Integer, nullable=False)
    even_strength_goals = Column(Integer, nullable=False)
    power_play_goals = Column(Integer, nullable=False)
    shorthanded_goals = Column(Integer, nullable=False)
    game_winning_goals = Column(Integer, nullable=False)
    even_strength_assists = Column(Integer, nullable=False)
    power_play_assists = Column(Integer, nullable=False)
    shorthanded_assists = Column(Integer, nullable=False)
    shots_on_goal = Column(Integer, nullable=False)
    shooting_pct = Column(Float, nullable=True, default=0.00)
    shifts = Column(Integer, nullable=False)
    time_on_ice = Column(Integer, nullable=False)
    hits = Column(Integer, nullable=False)
    blocks = Column(Integer, nullable=False)
    faceoff_wins = Column(Integer, nullable=False)
    faceoff_losses = Column(Integer, nullable=False)
    faceoff_pct = Column(Float, nullable=True)

    def __repr__(self):
        return f"<LastFiveGames {self.player_name}>"


class AllGames(Base, SerializerMixin):
    __tablename__ = "all_games"
    metadata = sqlalchemy.MetaData()

    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(DateTime, nullable=False)
    home = Column(String, nullable=False)
    home_goals = Column(Integer, nullable=False)
    visitor = Column(String, nullable=False)
    visitor_goals = Column(Integer, nullable=False)


class BasicTeamStats(Base, SerializerMixin):
    __tablename__ = "basic_team_stats"
    metadata = sqlalchemy.MetaData()

    id = Column(Integer, primary_key=True, autoincrement=True)
    year = Column(Integer, nullable=False)
    team = Column(String, nullable=False)
    average_age = Column(Float, nullable=False)
    games_played = Column(Integer, nullable=False)
    wins = Column(Integer, nullable=False)
    losses = Column(Integer, nullable=False)
    overtime_losses = Column(Integer, nullable=False)
    points = Column(Integer, nullable=False)
    point_pct = Column(Float, nullable=False)
    goals_for = Column(Integer, nullable=False)
    goals_against = Column(Integer, nullable=False)
    simple_rating_system = Column(Float, nullable=False)
    strength_of_schedule = Column(Float, nullable=False)
    total_goals_per_game = Column(Float, nullable=False)
    power_play_goals = Column(Integer, nullable=False)
    power_play_opportunities = Column(Integer, nullable=False)
    power_play_pct = Column(Float, nullable=False)
    power_play_goals_against = Column(Integer, nullable=False)
    power_play_opportunities_against = Column(Integer, nullable=False)
    penalty_kill_pct = Column(Float, nullable=False)
    shorthanded_goals = Column(Integer, nullable=False)
    shorthanded_goals_against = Column(Integer, nullable=False)
    shots_on_goal = Column(Integer, nullable=False)
    shooting_pct = Column(Float, nullable=False)
    shots_against = Column(Integer, nullable=False)
    save_pct = Column(Float, nullable=False)
    pdo = Column(Float, nullable=False)
    shutouts = Column(Integer, nullable=False)


class BasicCareerStats(Base, SerializerMixin):
    __tablename__ = "career_basic_stats"
    metadata = sqlalchemy.MetaData()

    id = Column(Integer, autoincrement=True, primary_key=True)
    player_id = Column(Integer, nullable=False)
    player_name = Column(String, nullable=False)
    season = Column(String, nullable=False)
    age = Column(Integer, nullable=False)
    team_id = Column(String, nullable=False)
    lg_id = Column(String, nullable=False)
    games_played = Column(Integer, nullable=False)
    goals = Column(Integer, nullable=False)
    assists = Column(Integer, nullable=False)
    points = Column(Integer, nullable=False)
    plus_minus = Column(Integer, nullable=False)
    pen_min = Column(Integer, nullable=False)
    goals_ev = Column(Integer, nullable=False)
    goals_pp = Column(Integer, nullable=False)
    goals_sh = Column(Integer, nullable=False)
    goals_gw = Column(Integer, nullable=False)
    assists_ev = Column(Integer, nullable=False)
    assists_pp = Column(Integer, nullable=False)
    assists_sh = Column(Integer, nullable=False)
    shots = Column(Integer, nullable=False)
    shot_pct = Column(Float, nullable=False)
    shots_attempted = Column(Integer, nullable=True, default=0)
    time_on_ice = Column(Integer, nullable=True)
    time_on_ice_avg = Column(String, nullable=True)
    faceoff_wins = Column(Integer, nullable=True)
    faceoff_losses = Column(Integer, nullable=True)
    faceoff_percentage = Column(Float, nullable=True)
    blocks = Column(Integer, nullable=True)
    hits = Column(Integer, nullable=True)
    takeaways = Column(Integer, nullable=True)
    giveaways = Column(Integer, nullable=True)
    award_summary = Column(String, nullable=True)


class PossessionCareerStats(Base, SerializerMixin):
    __tablename__ = "career_possession_stats"
    metadata = sqlalchemy.MetaData()

    id = Column(Integer, autoincrement=True, primary_key=True)
    player_id = Column(Integer, nullable=False)
    player_name = Column(String, nullable=False)
    season = Column(String, nullable=True)
    age = Column(Integer, nullable=True)
    team_name = Column(String, nullable=True)
    lg_id = Column(String, nullable=False)
    games_played = Column(Integer, nullable=False)
    toi_pbp_ev = Column(Float, nullable=False)
    corsi_for_ev = Column(Integer, nullable=False)
    corsi_against_ev = Column(Integer, nullable=False)
    corsi_pct_ev  = Column(Float, nullable=False)
    corsi_rel_pct_ev = Column(Float, nullable=False)
    fenwick_for_ev = Column(Integer, nullable=False)
    fenwick_against_ev = Column(Integer, nullable=False)
    fenwick_pct_ev = Column(Float, nullable=False)
    fenwick_rel_pct_ev = Column(Float, nullable=False)
    on_goals_ev = Column(Integer, nullable=False)
    on_ice_shot_pct_ev = Column(Float, nullable=False)
    on_goals_against_ev = Column(Integer, nullable=False)
    on_ice_sv_pct_ev = Column(Float, nullable=False)
    pdo_ev = Column(Float, nullable=False)
    zs_offense_pct_ev = Column(Float, nullable=False)
    zs_defense_pct_ev = Column(Float, nullable=False)


class MiscStats(Base, SerializerMixin):
    __tablename__ = "nhl_player_misc"
    metadata = sqlalchemy.MetaData()

    id = Column(Integer, autoincrement=True, primary_key=True)
    player_id = Column(Integer, nullable=False)
    player_name = Column(String, nullable=False)
    season = Column(String, nullable=False)
    age = Column(Integer, nullable=False)
    team_id = Column(String, nullable=False)
    lg_id = Column(String, nullable=False)
    games_played = Column(Integer, nullable=False)
    goals_created = Column(Integer, nullable=False)
    goals_per_game = Column(Float, nullable=True)
    assists_per_game = Column(Float, nullable=True)
    points_per_game = Column(Float, nullable=True)
    goals_created_per_game = Column(Float, nullable=True)
    penalty_minutes_per_game = Column(Float, nullable=True)
    shots_per_game = Column(Float, nullable=True)
    goals_adjusted = Column(Integer, nullable=True)
    assists_adjusted = Column(Integer, nullable=True)
    points_adjusted = Column(Integer, nullable=True)
    goals_created_adjusted = Column(Integer, nullable=True)
    total_goals_for_on_ice = Column(Integer, nullable=True)
    power_play_goals_for_on_ice = Column(Integer, nullable=True)
    total_goals_against_on_ice = Column(Integer, nullable=True)
    power_play_goals_against_on_ice = Column(Integer, nullable=True)
    plus_minus = Column(Integer, nullable=True)
    offensive_point_shares = Column(Float, nullable=True)
    defensive_point_shares = Column(Float, nullable=True)
    total_point_shares = Column(Float, nullable=True)
    shootout_attempts = Column(Integer, nullable=True, default=None)
    shootout_made = Column(Integer, nullable=True, default=None)
    shootout_miss = Column(Integer, nullable=True, default=None)
    shootout_pct = Column(Float, nullable=True, default=None)
    expected_goals_for = Column(Float, nullable=True, default=None)
    expected_goals_against = Column(Float, nullable=True, default=None)
    expected_plus_minus = Column(Float, nullable=True, default=None)

