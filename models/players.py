from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, DateTime
from sqlalchemy_serializer import SerializerMixin
import sqlalchemy

Base = declarative_base()


class Players(Base, SerializerMixin):

    __tablename__ = "players"
    metadata = sqlalchemy.MetaData()

    id = Column(Integer, autoincrement=True, unique=True, nullable=False, primary_key=True)
    player_id = Column(Integer, unique=True, nullable=False)
    player_url = Column(String, unique=True, nullable=False)
    has_advanced = Column(Boolean, unique=False, nullable=True)
    name = Column(String, unique=False, nullable=True)

    def __repr__(self):
        return "<Player: {player_id}>".format(player_id=self.player_id)

