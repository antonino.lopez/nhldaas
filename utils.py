from typing import List
import re
import pandas as pd
import numpy as np
import requests


def any_floats_in_list(lst: List) -> bool:
    bool_list = []
    for item in lst:
        try:
            float(item)
            bool_list.append(True)

        except ValueError:
            bool_list.append(False)
    return any(item for item in bool_list)


def find_row_cutoff(lst: List) -> int:
    for idx, item in enumerate(lst):
        if not idx:
            continue
        if bool(re.search(r'\d\d\d\d-\d\d', item)):
            return idx
    return len(lst)


def transform_dict_to_records(dic):
    records = pd.DataFrame(dic).replace(r'^\s*$', np.nan, regex=True).to_dict('records')
    return records


def remove_html_comments(html: requests.Response.text) -> str:
    player_html = re.sub("(<!--|-->)", "", html, flags=re.DOTALL)
    return player_html
