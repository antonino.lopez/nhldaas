FROM python:3.7-slim
RUN apt-get update && apt-get install -y curl locales locales-all gcc

ADD . /app
WORKDIR /app

ENV PYTHONUNBUFFERED 1
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

ENV PYTHONPATH=.:database:models:scrapers:scripts;

WORKDIR /app
EXPOSE 5000

CMD ["/bin/bash", "./scripts/restore_db.sh"]
CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "5000", "--reload"]

