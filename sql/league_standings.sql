create table league_standings
(
    rank                              integer          not null,
    team                              varchar(50),
    avg_age                           double precision not null,
    games_played                      integer          not null,
    wins                              integer          not null,
    losses                            integer          not null,
    overtime_losses                   integer          not null,
    points                            integer          not null,
    points_pct                        double precision not null,
    goals_for                         integer          not null,
    goals_against                     integer          not null,
    shootout_wins                     integer          not null,
    shootout_losses                   integer          not null,
    simple_rating_system              double precision not null,
    strength_of_schedule              double precision not null,
    total_goals_per_game              double precision not null,
    even_strength_goals_for           integer          not null,
    even_strength_goals_against       integer          not null,
    power_play_goals_for              integer          not null,
    power_play_opportunities_for      integer          not null,
    power_play_pct                    double precision not null,
    power_play_goals_against          integer          not null,
    power_play_opportunities_against  integer          not null,
    penalty_kill_pct                  double precision not null,
    short_handed_goals_for            integer          not null,
    short_handed_goals_against        integer          not null,
    penalty_minutes_per_game          double precision not null,
    opponent_penalty_minutes_per_game double precision not null,
    shots                             integer          not null,
    shooting_pct                      double precision not null,
    shots_against                     integer          not null,
    save_pct                          double precision not null,
    shutouts                          integer          not null,
    year                              integer          not null,
    id                                serial           not null
        constraint league_standings_pkey
            primary key
);

alter table league_standings
    owner to postgres;

