create table basic_team_stats
(
    year                             integer          not null,
    team                             varchar(50)      not null,
    average_age                      double precision not null,
    games_played                     integer          not null,
    wins                             integer          not null,
    losses                           integer          not null,
    overtime_losses                  integer          not null,
    points                           integer          not null,
    point_pct                        double precision not null,
    goals_for                        integer          not null,
    goals_against                    integer          not null,
    simple_rating_system             double precision not null,
    strength_of_schedule             double precision not null,
    total_goals_per_game             double precision not null,
    power_play_goals                 integer          not null,
    power_play_opportunities         integer          not null,
    power_play_pct                   double precision not null,
    power_play_goals_against         integer          not null,
    power_play_opportunities_against integer          not null,
    penalty_kill_pct                 double precision not null,
    shorthanded_goals                integer          not null,
    shorthanded_goals_against        integer          not null,
    shots_on_goal                    integer          not null,
    shooting_pct                     double precision not null,
    shots_against                    integer          not null,
    save_pct                         double precision not null,
    pdo                              double precision not null,
    shutouts                         integer          not null,
    id                               serial           not null
        constraint basic_team_stats_pk
            primary key
);

alter table basic_team_stats
    owner to postgres;

create unique index basic_team_stats_id_uindex
    on basic_team_stats (id);
