create table division_standings
(
    team                   varchar(50),
    games_played           integer          not null,
    wins                   integer          not null,
    losses                 integer          not null,
    overtime_losses        integer          not null,
    points                 integer          not null,
    points_pct             double precision not null,
    goals_for              integer          not null,
    goals_against          integer          not null,
    simple_rating_system   double precision not null,
    strength_of_schedule   double precision not null,
    points_pct_no_ot       double precision not null,
    wins_in_regulation     integer          not null,
    regulation_time_record varchar(10)      not null,
    point_pct_in_reg_time  double precision not null,
    division               varchar(50)      not null,
    year                   integer          not null,
    id                     serial           not null
        constraint division_standings_pkey
            primary key
);

alter table division_standings
    owner to postgres;
