create table all_games
(
    id            serial      not null
        constraint all_games_pkey
            primary key,
    date          date        not null,
    home          varchar(30) not null,
    home_goals    integer,
    visitor       varchar(30) not null,
    visitor_goals integer
);

alter table all_games
    owner to postgres;
