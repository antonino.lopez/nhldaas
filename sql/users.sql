create table users
(
    id           integer     not null
        constraint users_pkey
            primary key,
    username     varchar(15) not null,
    email        varchar(200),
    password     varchar(24) not null,
    is_active    boolean     not null,
    subscription varchar(30) not null
);

alter table users
    owner to trident;