create table player_last_five_games
(
    player_id             integer     not null,
    player_name           varchar(30) not null,
    date                  varchar(10) not null,
    team                  varchar(10) not null,
    opponent              varchar(10) not null,
    outcome               varchar(10) not null,
    goals                 integer     not null,
    assists               integer     not null,
    points                integer     not null,
    plus_minus            integer     not null,
    penalty_minutes       integer     not null,
    even_strength_goals   integer     not null,
    power_play_goals      integer     not null,
    shorthanded_goals     integer     not null,
    game_winning_goals    integer     not null,
    even_strength_assists integer     not null,
    power_play_assists    integer     not null,
    shorthanded_assists   integer     not null,
    shots_on_goal         integer     not null,
    shooting_pct          double precision,
    shifts                integer     not null,
    time_on_ice           varchar(10) not null,
    hits                  integer     not null,
    blocks                integer     not null,
    faceoff_wins          integer     not null,
    faceoff_losses        integer     not null,
    faceoff_pct           double precision
);

alter table player_last_five_games
    owner to postgres;

