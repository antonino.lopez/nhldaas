from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from starlette.config import Config


class MetaProcess:
    # set this up later
    # config = Config("LOCAL_DB.env")
    DATABASE_URL = "postgresql://postgres@localhost:5432/nhl_develop"
    engine = create_engine(DATABASE_URL)
    Session = sessionmaker(bind=engine)
    db_session = Session()
    base_url = "https://www.hockey-reference.com"
