
class DataIntegrityError(Exception):
    def __init__(self, error):
        super(DataIntegrityError, self).__init__("".format(error))
