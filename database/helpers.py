from sqlalchemy import create_engine
import io
import pandas as pd
from scrapers.nhl.standings import all_standings


def dict_of_lists_to_models(dict_of_lists, model, player_name=None):
    models = []
    model_instance = model
    for dic in dict_of_lists:
        for k, v in dic.items():
            model_instance.k = v
        if player_name:
            model_instance.player_name = player_name
        models.append(model_instance)

    return models
