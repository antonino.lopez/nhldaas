from sqlalchemy import create_engine


db_string = "postgresql://localhost:5432/nhl_develop"
db = create_engine(db_string)


def create_player_stats_table(player_name: str):
    create_table = f"""
    CREATE TABLE {player_name}(
       id SERIAL NOT NULL,
       current_team VARCHAR(100) NOT NULL,
       current_division VARCHAR (50) NOT NULL,
    )
    """
    db.execute(create_table)


def create_last_5_table(player_name: str):
    create_table = f"""
    CREATE TABLE {player_name}_last_5 (
        player_id int NOT NULL,
        date varchar (10) NOT NULL,
        team varchar (10) NOT NULL,
        opponent varchar (10) NOT NULL,
        outcome varchar (10) NOT NULL,
        goals INTEGER NOT NULL,
        assists INTEGER NOT NULL,
        points INTEGER NOT NULL,
        plus_minus INTEGER NOT NULL,
        penalty_minutes INTEGER NOT NULL,
        even_strength_goals INTEGER NOT NULL,
        power_play_goals INTEGER NOT NULL,
        shorthanded_goals INTEGER NOT NULL,
        game_winning_goals INTEGER NOT NULL,
        even_strength_assists INTEGER NOT NULL,
        power_play_assists INTEGER NOT NULL,
        shorthanded_assists INTEGER NOT NULL,
        shots_on_goal INTEGER NOT NULL,
        shooting_pct FLOAT,
        shifts INTEGER NOT NULL,
        time_on_ice varchar (10) NOT NULL,
        hits INTEGER NOT NULL,
        blocks INTEGER NOT NULL,
        faceoff_wins INTEGER NOT NULL,
        faceoff_losses INTEGER NOT NULL,
        faceoff_pct FLOAT
    )
    """
    db.execute(create_table)


create_league_standings_table = (
    """
    CREATE TABLE league_standings(
        rank INTEGER NOT NULL,
        team VARCHAR (50),
        avg_age FLOAT NOT NULL,
        games_played INTEGER NOT NULL,
        wins INTEGER NOT NULL,
        losses INTEGER NOT NULL,
        overtime_losses INTEGER NOT NULL,
        points INTEGER NOT NULL,
        points_pct FLOAT NOT NULL,
        goals_for INTEGER NOT NULL,
        goals_against INTEGER NOT NULL,
        shootout_wins INTEGER NOT NULL,
        shootout_losses INTEGER NOT NULL,
        simple_rating_system FLOAT NOT NULL,
        strength_of_schedule FLOAT NOT NULL,
        total_goals_per_game FLOAT NOT NULL,
        even_strength_goals_for INTEGER NOT NULL,
        even_strength_goals_against INTEGER NOT NULL,
        power_play_goals_for INTEGER NOT NULL,
        power_play_opportunities_for INTEGER NOT NULL,
        power_play_pct FLOAT NOT NULL,
        power_play_goals_against INTEGER NOT NULL,
        power_play_opportunities_against INTEGER NOT NULL,
        penalty_kill_pct FLOAT NOT NULL,
        short_handed_goals_for INTEGER NOT NULL,
        short_handed_goals_against INTEGER NOT NULL,
        penalty_minutes_per_game FLOAT NOT NULL,
        opponent_penalty_minutes_per_game FLOAT NOT NULL,
        shots INTEGER NOT NULL,
        shooting_pct FLOAT NOT NULL,
        shots_against INTEGER NOT NULL,
        save_pct FLOAT NOT NULL,
        shutouts INTEGER NOT NULL,
        year INTEGER NOT NULL
    )
    """
)


create_division_standings_table = (
    """
    CREATE TABLE division_standings(
        team VARCHAR (50),
        games_played INTEGER NOT NULL,
        wins INTEGER NOT NULL,
        losses INTEGER NOT NULL,
        overtime_losses INTEGER NOT NULL,
        points INTEGER NOT NULL,
        points_pct FLOAT NOT NULL,
        goals_for INTEGER NOT NULL,
        goals_against INTEGER NOT NULL,
        simple_rating_system FLOAT NOT NULL,
        strength_of_schedule FLOAT NOT NULL,
        points_pct_no_ot FLOAT NOT NULL,
        wins_in_regulation INTEGER NOT NULL,
        regulation_time_record VARCHAR (10) NOT NULL,
        point_pct_in_reg_time FLOAT NOT NULL,
        division VARCHAR (50) NOT NULL,
        year INTEGER NOT NULL
    )
    """
)


create_users_table = (
    """
    CREATE TABLE users(
        id INTEGER PRIMARY KEY,
        username VARCHAR (15) NOT NULL,
        email VARCHAR (200),
        password VARCHAR (24) NOT NULL,
        is_active BOOLEAN NOT NULL
    )
    """
)


if __name__ == "__main__":
    db.execute(create_league_standings_table)
