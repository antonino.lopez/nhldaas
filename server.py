from starlette.applications import Starlette
from starlette.config import Config
from starlette.responses import JSONResponse
from starlette.routing import Route
from starlette.exceptions import HTTPException
from starlette.authentication import AuthenticationBackend, AuthenticationError, SimpleUser, UnauthenticatedUser, \
    AuthCredentials
from starlette.middleware.authentication import AuthenticationMiddleware
from models.nhl_models import DivisionStandings, LeagueStandings, PossessionCareerStats, BasicCareerStats, MiscStats

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from encoders import AlchemyEncoder
import json

config = Config(".env")
DATABASE_URL = config("DATABASE_URL")
engine = create_engine(DATABASE_URL)
Session = sessionmaker(bind=engine)
session = Session()


async def get_player(request):
    player_id = request.path_params.get("player_id", None)
    if player_id:
        player_stats = session.query(PossessionCareerStats, BasicCareerStats, MiscStats) \
            .filter(PossessionCareerStats.player_id == player_id).all()

    else:
        raise HTTPException(status_code=404, detail="Please provide a player id.")

    return JSONResponse([player_stat.to_dict() for player_stat in player_stats])


async def get_player_possession(request):
    player_id = request.path_params.get("player_id", None)
    if player_id:
        player_stats = session.query(PossessionCareerStats).filter(PossessionCareerStats.player_id == player_id).all()

    else:
        raise HTTPException(status_code=404, detail="Please provide a player id.")

    return JSONResponse([player_stat.to_dict() for player_stat in player_stats])


async def get_division_standings(request):
    year = request.path_params.get("year", None)
    if year:
        results = session.query(DivisionStandings).filter(DivisionStandings.year == year).all()
    else:
        results = session.query(DivisionStandings).all()

    return JSONResponse([result.to_dict() for result in results])


async def get_league_standings(request):
    year = request.path_params.get("year", None)
    if year:
        results = session.query(LeagueStandings).filter(LeagueStandings.year == year).all()
    else:
        results = session.query(LeagueStandings).all()

    return JSONResponse([result.to_dict() for result in results])


async def http_exception(request, exc):
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)


exception_handlers = {
    HTTPException: http_exception
}

routes = [
    Route("/standings/divisions/", endpoint=get_division_standings, methods=['GET']),
    Route("/standings/divisions/{year:int}", endpoint=get_division_standings, methods=['GET']),
    Route("/standings/league/", endpoint=get_league_standings, methods=['GET']),
    Route("/standings/league/{year:int}", endpoint=get_league_standings, methods=['GET']),
    Route("/players/nhl/", endpoint=get_player_possession, methods=['GET']),
    Route("/players/nhl/{player_id:int}", endpoint=get_player, methods=['GET']),
    Route("/players/nhl/possession/{player_id:int}", endpoint=get_player_possession, methods=['GET']),
]

app = Starlette(
    debug=True,
    routes=routes,
)
