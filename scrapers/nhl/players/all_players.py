import requests
from bs4 import BeautifulSoup
import pandas as pd
from typing import List, Dict
import string
import json
from meta import MetaProcess
from models.nhl_models import LastFiveGames, AllGames, BasicCareerStats, PossessionCareerStats, MiscStats
from utils import any_floats_in_list, find_row_cutoff, remove_html_comments
from exceptions import DataIntegrityError
import numpy as np
import re
import time


class PlayerProcess(MetaProcess):

    def __init__(self):
        super(MetaProcess, self).__init__()
        self.player_base_url = "https://www.hockey-reference.com/players"
        self.base_url = "https://www.hockey-reference.com"
        self.all_player_ids = {}
        self.refresh = True

    def get_player_urls(self, letter: str) -> List:
        url = f"{self.player_base_url}/{letter}/"
        print(url)
        response = requests.get(url=url)
        soup = BeautifulSoup(response.content, "lxml")
        div = soup.find("div", attrs={"id": "div_players"})

        player_urls = []
        for a in div.find_all("a"):
            player_urls.append(a['href'])

        return player_urls

    def find_player_url(self, player: str) -> str:
        return self.base_url + player

    def refresh_player_urls_and_ids(self, advanced: bool):
        all_player_urls = []
        for letter in string.ascii_lowercase:
            if letter != "x":
                player_urls = self.get_player_urls(letter)
                all_player_urls.extend(player_urls)
        with open("player_urls.json", "w") as file:
            json.dump(all_player_urls, file)

        with open("player_ids.json", "w") as file:
            self.all_player_ids = {v: k + 1 for k, v in enumerate(all_player_urls)}
            json.dump(self.all_player_ids, file)

    @staticmethod
    def is_goalie(player_soup: BeautifulSoup):
        position = player_soup.find("div", attrs={"itemtype": "https://schema.org/Person"}).find("p").text
        position = "".join(c for c in position if c in string.ascii_letters)
        return True if "G" in position else False

    @staticmethod
    def transform_dict_to_records(dic) -> List[Dict]:
        records = pd.DataFrame(dic).replace(r'^\s*$', np.nan, regex=True).to_dict('records')
        return records

    @staticmethod
    def get_player_name(soup: BeautifulSoup) -> str:
        selector_player_name = soup.find("h1", attrs={"itemprop": "name"})
        player_name = selector_player_name.text.lower()
        return "_".join(player_name.split())

    def get_career_stats(self, soup: BeautifulSoup, player_id: int):
        table = soup.find("table", attrs={"id": "stats_basic_plus_nhl"})
        if not table:
            return
        elif self.is_goalie(player_soup=soup):
            return

        data = []
        for tr in table.find_all("tr"):
            season_val = tr.find("th").text
            if season_val == "Season" or season_val == "Career" or "yr" in season_val:
                continue
            else:
                if season_val:
                    data.append(season_val)
            for td in tr.find_all("td"):
                data.append(td.text)

        career_dict = {"season": data[0::30], "age": data[1::30], "team_id": data[2::30], "lg_id": data[3::30],
                       "games_played": data[4::30], "goals": data[5::30], "assists": data[6::30], "points": data[7::30],
                       "plus_minus": data[8::30], "pen_min": data[9::30], "goals_ev": data[10::30],
                       "goals_pp": data[11::30], "goals_sh": data[12::30], "goals_gw": data[13::30],
                       "assists_ev": data[14::30], "assists_pp": data[15::30], "assists_sh": data[16::30],
                       "shots": data[17::30], "shot_pct": data[18::30], "shots_attempted": data[19::30],
                       "time_on_ice": data[20::30], "time_on_ice_avg": data[21::30], "faceoff_wins": data[22::30],
                       "faceoff_losses": data[23::30], "faceoff_percentage": data[24::30], "blocks": data[25::30],
                       "hits": data[26::30], "takeaways": data[27::30], "giveaways": data[28::30],
                       "award_summary": data[29::30]}

        records = self.transform_dict_to_records(career_dict)
        player_name = self.get_player_name(soup=soup)
        for record in records:
            for key, val in record.items():
                if key != "award_summary" and str(val) == "nan":
                    record[key] = 0

            try:
                basic_career_stats = BasicCareerStats(**record,
                                                      player_name=player_name,
                                                      player_id=player_id)
                self.db_session.add(basic_career_stats)
                self.db_session.commit()
                self.db_session.flush()
            except Exception as e:
                self.db_session.rollback()
                continue

    def get_career_possession_stats(self, soup: BeautifulSoup, player_id: int):
        table = soup.find("table", attrs={"id": "skaters_advanced"})
        if not table:
            return
        elif self.is_goalie(player_soup=soup):
            return

        data = []
        for tr in table.find_all("tr"):
            season_val = tr.find("th").text
            if season_val == "Season" or season_val == "Career" or "yr" in season_val:
                continue
            else:
                if season_val:
                    data.append(season_val)
            for td in tr.find_all("td"):
                data.append(td.text)

        career_possession_dict = {"season": data[0::21], "age": data[1::21], "team_name": data[2::21],
                                   "lg_id": data[3::21], "games_played": data[4::21], "toi_pbp_ev": data[5::21],
                                   "corsi_for_ev": data[6::21], "corsi_against_ev": data[7::21],
                                   "corsi_pct_ev": data[8::21], "corsi_rel_pct_ev": data[9::21],
                                   "fenwick_for_ev": data[10::21], "fenwick_against_ev": data[11::21],
                                   "fenwick_pct_ev": data[12::21], "fenwick_rel_pct_ev": data[13::21],
                                   "on_goals_ev": data[14::21], "on_ice_shot_pct_ev": data[15::21],
                                   "on_goals_against_ev": data[16::21], "on_ice_sv_pct_ev": data[17::21],
                                   "pdo_ev": data[18::21], "zs_offense_pct_ev": data[19::21],
                                   "zs_defense_pct_ev": data[20::21]}

        records = self.transform_dict_to_records(career_possession_dict)
        player_name = self.get_player_name(soup=soup)
        for record in records:
            for key, val in record.items():
                if key != "award_summary" and str(val) == "nan":
                    record[key] = 0

            try:
                career_possession_stats = PossessionCareerStats(**record, player_name=player_name,
                                                                player_id=player_id)
                self.db_session.add(career_possession_stats)
                self.db_session.commit()
                self.db_session.flush()

            except Exception as e:
                self.db_session.rollback()
                print(str(e))
                continue

    def get_misc_player_stats(self, soup: BeautifulSoup, player_id: int):
        table = soup.find("table", attrs={"id": "stats_misc_plus_nhl"})
        player_name = self.get_player_name(soup=soup)
        if not table:
            return
        elif self.is_goalie(player_soup=soup):
            return

        data = []
        for tr in table.find_all("tr"):
            season_val = tr.find("th").text
            if season_val == "Season" or season_val == "Career" or "yr" in season_val:
                continue
            else:
                if season_val:
                    data.append(season_val)
            for td in tr.find_all("td"):
                data.append(td.text)

        small_table_len = 24
        medium_table_len = 27
        sec_medium_table_len = 28
        large_table_len = 31

        try:
            cutoff = find_row_cutoff(data)
            # catch single row misc tables
            if cutoff == small_table_len:
                misc_stats = {"season": data[0::cutoff], "age": data[1::cutoff], "team_id": data[2::cutoff],
                              "lg_id": data[3::cutoff], "games_played": data[4::cutoff],
                              "goals_created": data[5::cutoff], "goals_per_game": data[6::cutoff],
                              "assists_per_game": data[7::cutoff], "points_per_game": data[8::cutoff],
                              "goals_created_per_game": data[9::cutoff], "penalty_minutes_per_game": data[10::cutoff],
                              "shots_per_game": data[11::cutoff], "goals_adjusted": data[12::cutoff],
                              "assists_adjusted": data[13::cutoff], "points_adjusted": data[14::cutoff],
                              "goals_created_adjusted": data[15::cutoff], "total_goals_for_on_ice": data[16::cutoff],
                              "power_play_goals_for_on_ice": data[17::cutoff],
                              "total_goals_against_on_ice": data[18::cutoff],
                              "power_play_goals_against_on_ice": data[19::cutoff], "plus_minus": data[20::cutoff],
                              "offensive_point_shares": data[21::cutoff], "defensive_point_shares": data[22::cutoff],
                              "total_point_shares": data[23::cutoff]}

            elif cutoff == medium_table_len or cutoff == sec_medium_table_len:
                misc_stats = {"season": data[0::cutoff], "age": data[1::cutoff], "team_id": data[2::cutoff],
                              "lg_id": data[3::cutoff], "games_played": data[4::cutoff],
                              "goals_created": data[5::cutoff], "goals_per_game": data[6::cutoff],
                              "assists_per_game": data[7::cutoff], "points_per_game": data[8::cutoff],
                              "goals_created_per_game": data[9::cutoff], "penalty_minutes_per_game": data[10::cutoff],
                              "shots_per_game": data[11::cutoff], "goals_adjusted": data[12::cutoff],
                              "assists_adjusted": data[13::cutoff], "points_adjusted": data[14::cutoff],
                              "goals_created_adjusted": data[15::cutoff], "total_goals_for_on_ice": data[16::cutoff],
                              "power_play_goals_for_on_ice": data[17::cutoff],
                              "total_goals_against_on_ice": data[18::cutoff],
                              "power_play_goals_against_on_ice": data[19::cutoff], "plus_minus": data[20::cutoff],
                              "offensive_point_shares": data[21::cutoff], "defensive_point_shares": data[22::cutoff],
                              "total_point_shares": data[23::cutoff], "expected_goals_for": data[24::cutoff],
                              "expected_goals_against": data[25::cutoff], "expected_plus_minus": data[26::cutoff]}

            elif cutoff == large_table_len:
                misc_stats = {"season": data[0::cutoff], "age": data[1::cutoff], "team_id": data[2::cutoff],
                              "lg_id": data[3::cutoff], "games_played": data[4::cutoff],
                              "goals_created": data[5::cutoff], "goals_per_game": data[6::cutoff],
                              "assists_per_game": data[7::cutoff], "points_per_game": data[8::cutoff],
                              "goals_created_per_game": data[9::cutoff], "penalty_minutes_per_game": data[10::cutoff],
                              "shots_per_game": data[11::cutoff], "goals_adjusted": data[12::cutoff],
                              "assists_adjusted": data[13::cutoff], "points_adjusted": data[14::cutoff],
                              "goals_created_adjusted": data[15::cutoff], "total_goals_for_on_ice": data[16::cutoff],
                              "power_play_goals_for_on_ice": data[17::cutoff],
                              "total_goals_against_on_ice": data[18::cutoff],
                              "power_play_goals_against_on_ice": data[19::cutoff], "plus_minus": data[20::cutoff],
                              "offensive_point_shares": data[21::cutoff], "defensive_point_shares": data[22::cutoff],
                              "total_point_shares": data[23::cutoff], "shootout_attempts": data[24::cutoff],
                              "shootout_made": data[25::cutoff], "shootout_miss": data[26::cutoff],
                              "shootout_pct": data[27::cutoff], "expected_goals_for": data[28::cutoff],
                              "expected_goals_against": data[29::cutoff], "expected_plus_minus": data[30::cutoff]}

            else:
                raise DataIntegrityError

            records = self.transform_dict_to_records(dic=misc_stats)
            for record in records:
                for key, val in record.items():
                    if str(val) == "nan":
                        record[key] = 0
                try:
                    misc_stats_record = MiscStats(**record, player_name=player_name,
                                                  player_id=player_id)
                    self.db_session.add(misc_stats_record)
                    self.db_session.flush()
                    self.db_session.commit()

                except Exception as e:
                    print(str(e))
                    self.db_session.rollback()

        except Exception as e:
            print(str(e))

    def get_last_five_games(self, soup: BeautifulSoup, player_id: int) -> None:
        last_5_dict = {}
        last_5_table = soup.find("table", attrs={"id": "last5"})
        try:
            data = []

            for trs in last_5_table.find_all("tr"):
                for td in trs.find_all("td"):
                    data.append(td.text)

            last_5_dict["date"] = data[0::26][:-1]
            last_5_dict["team"] = data[1::26]
            last_5_dict["opponent"] = data[3::26]
            last_5_dict["outcome"] = data[4::26]
            last_5_dict["goals"] = data[5::26]
            last_5_dict["assists"] = data[6::26]
            last_5_dict["points"] = data[7::26]
            last_5_dict["plus_minus"] = data[8::26]
            last_5_dict["penalty_minutes"] = data[9::26]
            last_5_dict["even_strength_goals"] = data[10::26]
            last_5_dict["power_play_goals"] = data[11::26]
            last_5_dict["shorthanded_goals"] = data[12::26]
            last_5_dict["game_winning_goals"] = data[13::26]
            last_5_dict["even_strength_assists"] = data[14::26]
            last_5_dict["power_play_assists"] = data[15::26]
            last_5_dict["shorthanded_assists"] = data[16::26]
            last_5_dict["shots_on_goal"] = data[17::26]
            last_5_dict["shooting_pct"] = data[18::26]
            last_5_dict["shifts"] = data[19::26]
            last_5_dict["time_on_ice"] = data[20::26]
            last_5_dict["hits"] = data[21::26]
            last_5_dict["blocks"] = data[22::26]
            last_5_dict["faceoff_wins"] = data[23::26]
            last_5_dict["faceoff_losses"] = data[24::26]
            last_5_dict["faceoff_pct"] = data[25::26]
            player_name = self.get_player_name(soup=soup)

            records = self.transform_dict_to_records(dic=last_5_dict)
            for record in records:
                last_five_games = LastFiveGames(**record, player_id=player_id, player_name=player_name)
                self.db_session.add(last_five_games)
                self.db_session.flush()
                self.db_session.commit()

        except Exception as e:
            print(str(e))

    def main(self) -> bool:
        if self.refresh:
            self.refresh_player_urls_and_ids()
        else:
            with open("player_urls.json") as file:
                all_player_urls = json.load(file)
            with open("player_ids.json") as file:
                all_player_ids = json.load(file)

        for player_url in all_player_urls:
            print(player_url)
            try:
                full_player_url = f"{self.base_url}{player_url}"
                response = requests.get(url=full_player_url)

            except requests.exceptions.ConnectionError as e:
                print(str(e))
                time.sleep(60)
                full_player_url = f"{self.base_url}{player_url}"
                response = requests.get(url=full_player_url)

            clean_html = remove_html_comments(response.text)
            soup = BeautifulSoup(clean_html, "lxml")
            player_id = all_player_ids[player_url]
            # self.get_last_five_games(soup, player_id)
            self.get_career_stats(soup, player_id)
            self.get_career_possession_stats(soup, player_id)
            self.get_misc_player_stats(soup, player_id)

        return True


if __name__ == "__main__":
    player_process = PlayerProcess()
    player_process.main()
