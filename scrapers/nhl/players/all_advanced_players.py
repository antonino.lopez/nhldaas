import requests
from bs4 import BeautifulSoup
import pandas as pd
from typing import List, Dict
import string
import json
from meta import MetaProcess
from .all_players import PlayerProcess
from utils import remove_html_comments
from exceptions import DataIntegrityError
import numpy as np
import re
import time


class AdvancedPlayerStats(PlayerProcess):

    def __init__(self):
        super(PlayerProcess, self).__init__()
        self.player_base_url = "https://www.hockey-reference.com/players"
        self.base_url = "https://www.hockey-reference.com"
        self.all_player_ids = {}
        self.refresh = False

    @staticmethod
    def make_advanced_from_player_url(player_urls: List) -> List:
        advanced_player_urls = []
        advanced_player_urls.extend(["-advanced.".join(player_url.split(".")) for player_url in player_urls])
        # advanced_player_urls.extend(["-advanced-pp.".join(player_url.split(".")) for player_url in player_urls])
        # advanced_player_urls.extend(["-advanced-sh.".join(player_url.split(".")) for player_url in player_urls])
        # advanced_player_urls.extend(["-advanced-5on5.".join(player_url.split(".")) for player_url in player_urls])
        # advanced_player_urls.extend(["-advanced-5on5close.".join(player_url.split(".")) for player_url in player_urls])
        # advanced_player_urls.extend(["-advanced-5on5tied.".join(player_url.split(".")) for player_url in player_urls])
        return advanced_player_urls

    def main(self):
        if self.refresh:
            self.refresh_player_urls_and_ids()
        else:
            with open("player_urls.json") as file:
                all_player_urls = json.load(file)
            with open("player_ids.json") as file:
                all_player_ids = json.load(file)
        advanced_player_urls = self.make_advanced_from_player_url(all_player_urls)
        print(len(advanced_player_urls))


if __name__ == "__main__":
    p = AdvancedPlayerStats()
    p.main()