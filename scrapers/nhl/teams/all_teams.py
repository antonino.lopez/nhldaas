import asyncio
from bs4 import BeautifulSoup
from meta import MetaProcess
from models.nhl_models import BasicTeamStats
import itertools
import aiohttp


class TeamProcess(MetaProcess):
    def __init__(self):
        self.team_abbreviations = {"BOS": "Boston Bruins", "ANA": "Anaheim Ducks", "COL": "Colorado Avalanche",
                                   "STL": "Saint Louis Blues", "SJS": "San Jose Sharks", "PIT": "Pittsburgh Penguins",
                                   "CHI": "Chicago Blackhawks", "TBL": "Tampa Bay Lightning", "LAK": "Los Angeles Kings"
                                   , "MTL": "Montreal Canadiens", "MIN": "Minnesota Wild", "NYR": "New York Rangers",
                                   "PHI": "Philadelphia Flyers", "CBJ": "Columbus Blue Jackets",
                                   "DET": "Detroit Red Wings", "DAL": "Dallas Stars", "WSH": "Washington Capitals",
                                   "PHX": "Phoenix Coyotes", "ARI": "Arizona Coyotes", "OTT": "Ottawa Senators",
                                   "NSH": "Nashville Predators", "NJD": "New Jersey Devils",
                                   "TOR": "Toronto Maple Leafs", "WPG": "Winnipeg Jets", "CAR": "Carolina Hurricanes",
                                   "VAN": "Vancouver Canucks","NYI": "New York Islanders", "CGY": "Calgary Flames",
                                   "EDM": "Edmonton Oilers", "FLA": "Florida Panthers", "BUF": "Buffalo Sabres"}
        self.base_urls = self.get_team_urls()
        self.all_team_stats = []

    def get_team_urls(self):
        years = range(2009, 2021)
        team_and_years = itertools.product(self.team_abbreviations.keys(), years)
        team_urls = []
        for team, year in team_and_years:
            if team == "PHX" and year in range(2015, 2030) or team == "ARI" and year in range(2004, 2013) \
                    or team == "WPG" and year in range(2009, 2012):
                continue
            team_urls.append(f"https://www.hockey-reference.com/teams/{team}/{year}.html")
        return team_urls

    async def parse_basic_team_data(self, team_data, url):
        meta_data = url.split("/")
        team = self.team_abbreviations[meta_data[4]]
        year = meta_data[-1][:4]
        soup = BeautifulSoup(team_data, "lxml")
        team_stats_table = soup.find("table", attrs={"id": "team_stats"})
        data = []
        try:
            for tr in team_stats_table.find_all('tr'):
                for td in tr.find_all("td"):
                    data.append(td.text)
            data_no_team_avg = data[:26]
            team_stats_dict = {"average_age": data_no_team_avg[0], "games_played": data_no_team_avg[1],
                               "wins": data_no_team_avg[2], "losses": data_no_team_avg[3],
                               "overtime_losses": data_no_team_avg[4], "points": data_no_team_avg[5],
                               "point_pct": data_no_team_avg[6], "goals_for": data_no_team_avg[7],
                               "goals_against": data_no_team_avg[8], "simple_rating_system": data_no_team_avg[9],
                               "strength_of_schedule": data_no_team_avg[10],
                               "total_goals_per_game": data_no_team_avg[11], "power_play_goals": data_no_team_avg[12],
                               "power_play_opportunities": data_no_team_avg[13], "power_play_pct": data_no_team_avg[14],
                               "power_play_goals_against": data_no_team_avg[15],
                               "power_play_opportunities_against": data_no_team_avg[16],
                               "penalty_kill_pct": data_no_team_avg[17], "shorthanded_goals": data_no_team_avg[18],
                               "shorthanded_goals_against": data_no_team_avg[19], "shots_on_goal": data_no_team_avg[20],
                               "shooting_pct": data_no_team_avg[21], "shots_against": data_no_team_avg[22],
                               "save_pct": data_no_team_avg[23], "pdo": data_no_team_avg[24],
                               "shutouts": data_no_team_avg[25], 'team': team, "year": year}
            print(team_stats_dict)
            self.all_team_stats.append(team_stats_dict)

        except Exception as e:
            print("This one:", url)

    async def get_basic_team_data(self, url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                data = await response.text()
                new_data = await self.parse_basic_team_data(team_data=data, url=url)
                await asyncio.sleep(3)

    def main(self):
        loop = asyncio.get_event_loop()
        urls = self.base_urls
        loop.run_until_complete(
            asyncio.gather(
                *(self.get_basic_team_data(url) for url in urls))
        )
        for team_stats in self.all_team_stats:
            basic_team_stats = BasicTeamStats(**team_stats)
            self.db_session.add(basic_team_stats)
            self.db_session.commit()


if __name__ == "__main__":
    process = TeamProcess()
    process.main()
