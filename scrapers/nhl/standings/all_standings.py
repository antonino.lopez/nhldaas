import re
import urllib.request
import requests
import pandas as pd
from meta import MetaProcess
from models.nhl_models import LeagueStandings, DivisionStandings, AllGames
from bs4 import BeautifulSoup
from sqlalchemy.inspection import inspect
from utils import transform_dict_to_records


class StandingsProcess(MetaProcess):

    def get_division_standings(self, year: int) -> None:

        url = f"https://www.hockey-reference.com/leagues/NHL_{str(year)}.html"
        html = pd.read_html(url)
        standings_df = html[1]
        df_rows = standings_df.values.tolist()

        central_pacific_rows = df_rows[1:8] + df_rows[9:]
        for idx, row in enumerate(central_pacific_rows):
            if idx in range(7):
                row.append("Central Division")
            else:
                row.append("Pacific Division")
            row.append(year)

        standings_df = html[0]
        df_rows = standings_df.values.tolist()
        atlantic_metro_rows = df_rows[1:9] + df_rows[10:]
        for idx, row in enumerate(atlantic_metro_rows):
            if idx in range(8):
                row.append('Atlantic Division')
            else:
                row.append('Metropolitan Division')
            row.append(year)

        atlantic_metro_df = pd.DataFrame(atlantic_metro_rows)
        central_pacific_df = pd.DataFrame(central_pacific_rows)
        divisions_df = pd.concat([atlantic_metro_df, central_pacific_df], axis=0)
        columns = [column.name for column in inspect(DivisionStandings).c][1:]
        divisions_df.columns = columns
        records = divisions_df.to_dict("records")

        for record in records:
            try:
                division_standings = DivisionStandings(**record)
                self.db_session.add(division_standings)
                self.db_session.flush()
                self.db_session.commit()

            except Exception as e:
                print(str(e))
                self.db_session.rollback()

    def get_all_standings(self, year: int) -> None:
        url = f"https://www.hockey-reference.com/leagues/NHL_{str(year)}.html"
        with urllib.request.urlopen(url=url) as response:
            html = response.read().decode("UTF-8")
        team_html = re.sub("(<!--|-->)", "", html, flags=re.DOTALL)
        soup = BeautifulSoup(team_html, "lxml")
        table = soup.find("table", attrs={"class": "sortable stats_table"})
        data = []
        for idx, tr in enumerate(table.find_all('tr')):
            if idx > 1:
                for th in tr.find_all("th"):
                    data.append(th.text)
            for td in tr.find_all("td"):
                data.append(td.text)

        columns = [column.name for column in inspect(LeagueStandings).c][1:-1]
        data = data[:-33]

        stats_dict = {}
        for idx, col in enumerate(columns):
            stats_dict[col] = data[idx::33]

        records = transform_dict_to_records(stats_dict)

        for record in records:
            try:
                league_standings = LeagueStandings(**record, year=year)
                self.db_session.add(league_standings)
                self.db_session.flush()
                self.db_session.commit()

            except Exception as e:
                print(str(e))
                self.db_session.rollback()

    def get_all_games(self):
        for year in range(2010, 2021):
            url = 'https://www.hockey-reference.com/leagues/NHL_' + str(year) + '_games.html'
            res = requests.get(url)
            soup = BeautifulSoup(res.content, 'html.parser')
            games_table = soup.find('table', {'class': 'sortable stats_table'})
            games_data = [td.text for td in games_table.find_all('td')]

            all_games_dict = {'date': [date.text for date in games_table.find_all('th')][9::],
                              'home': games_data[2::8], 'home_goals': games_data[3::8],
                              "visitor": games_data[0::8], "visitor_goals": games_data[1::8]}

            for idx, (h_goals, v_goals) in enumerate(
                    zip(all_games_dict['home_goals'], all_games_dict['visitor_goals'])):
                if h_goals == "" and v_goals == "":
                    all_games_dict['home_goals'][idx] = None
                    all_games_dict['visitor_goals'][idx] = None

            records = transform_dict_to_records(dic=all_games_dict)
            for record in records:
                all_games = AllGames(**record)
                self.db_session.add(all_games)
                self.db_session.flush()
                self.db_session.commit()

    def main(self):
        for year in range(2006, 2021):
            self.get_all_standings(year=year)

        for year in range(2014, 2021):
            self.get_division_standings(year=year)


if __name__ == "__main__":
    team_process = StandingsProcess()
    team_process.main()
