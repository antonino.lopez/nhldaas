from sqlalchemy import create_engine
from starlette.config import Config
import os
import logging
from datetime import datetime
from scrapers.nhl.players.all_players import PlayerProcess
from scrapers.nhl.teams.all_teams import TeamProcess
from scrapers.nhl.standings.all_standings import StandingsProcess


config = Config("../database/LOCAL_DB.env")
DATABASE_URL = config("DATABASE_URL")
engine = create_engine(DATABASE_URL)


sql = """
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
COMMENT ON SCHEMA public IS 'standard public schema';
"""

# drop all tables
logging.basicConfig(filename="../logs/refresh.log", level=logging.DEBUG)
run_start = str(datetime.now())
logging.debug(f"Refresh Start at: {run_start}")

with engine.connect() as conn:
    conn.execute(sql)
    logging.info("Tables were dropped successfully.")

    # create tables
    sql_directory = "../sql/"
    for root, dirs, files in os.walk(sql_directory):
        for file in files:
            file_path = os.path.join(root, file)
            with open(file_path, "r") as sql_file:
                conn.execute(sql_file.read())
                table_name = file_path.split("/")[-1].split(".")[0]
                logging.info(f"Successfully created table: {table_name}")

# create processes
logging.info("Running player process.")
player_process = PlayerProcess()
player_process.refresh = True
player_process.main()
logging.info("Successfully ran player process.")


logging.info("Running standings process.")
standings_process = StandingsProcess()
standings_process.main()
logging.info("Successfully ran standings process.")

logging.info("Running team process.")
team_process = TeamProcess()
team_process.main()
logging.info("Successfully ran team process.")