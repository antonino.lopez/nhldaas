import json
import os
import re
from meta import MetaProcess
from models.players import Players
from scrapers.nhl.players.all_advanced_players import AdvancedPlayerStats
import requests


class PlayerService(AdvancedPlayerStats):

    def __init__(self):
        super(AdvancedPlayerStats, self).__init__()
        self.player_id_file = "player_ids.json"

    @property
    def player_data(self):
        with open(self.player_id_file, "r") as player_file:
            player_ids = json.load(player_file)
        return player_ids

    def add_player_ids_names(self):
        for player_url, player_id in self.player_data.items():
            player = Players(player_id=player_id, player_url=player_url)
            print(player)
            self.db_session.add(player)
            self.db_session.flush()
            self.db_session.commit()

    def player_has_advanced(self):
        player_urls = [player_url for player_url, _ in self.player_data.items()]
        advanced_player_urls = self.make_advanced_from_player_url(player_urls)
        for idx, player_url in enumerate(advanced_player_urls):
            player = self.db_session.query(Players).filter(Players.player_url == player_urls[idx]).first()
            response = requests.get(self.base_url + player_url)
            if response.status_code == 200:
                player.has_advanced = True
                self.db_session.commit()
                print(player_url + ": ADV")

            elif response.status_code == 404:
                player.has_advanced = False
                self.db_session.commit()
                print(player_url + ": NON")

            else:
                print("Status Code: {} not handled.\nInfo: {}".format(response.status_code, response.text))
    
    def main(self):
        results = self.db_session.query(Players).all()
        print(len(results))


if __name__ == '__main__':
    player_service = PlayerService()
    player_service.main()
