#!/usr/bin/env bash

DOCKER_DB_NAME="$(docker-compose ps -q postgres)"
DB_HOSTNAME=postgres
DB_USER=postgres
DB_NAME=nhl_develop
LOCAL_DUMP_PATH="backup.sql"

docker-compose up -d postgres
docker exec -u postgres -i  "${DOCKER_DB_NAME}" createuser trident
docker exec -i "${DOCKER_DB_NAME}" dropdb -U "${DB_USER}" --if-exists "${DB_NAME}"
docker exec -i "${DOCKER_DB_NAME}" createdb -U "${DB_USER}" "${DB_NAME}"
docker cp "${LOCAL_DUMP_PATH}" postgres_service:/backups
docker exec -i "${DOCKER_DB_NAME}" psql -U "${DB_USER}" -v ON_ERROR_STOP=1 -d "${DB_NAME}" -1 -f "backups/${LOCAL_DUMP_PATH}"
docker-compose stop postgres