from starlette.applications import Starlette
from starlette.config import Config
from starlette.responses import JSONResponse
from starlette.routing import Route
import pandas as pd
from models.nhl_models import DivisionStandings, LeagueStandings, LastFiveGames, AllGames, BasicTeamStats, \
    BasicCareerStats, PossessionCareerStats

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

config = Config("database/LOCAL_DB.env")
DATABASE_URL = config("DATABASE_URL")

engine = create_engine(DATABASE_URL)
Session = sessionmaker(bind=engine)
session = Session()

all_tables = [DivisionStandings, LeagueStandings, LastFiveGames, AllGames, BasicTeamStats, BasicCareerStats,
              PossessionCareerStats]


async def get_total_rows(request):
    rows = 0
    for table in all_tables:
        rows += len(session.query(table).all())

    json = {"total_rows": rows}
    return JSONResponse(json)


routes = [
    Route("/v1/dashboard/total_rows", endpoint=get_total_rows, methods=['GET']),
]

app = Starlette(
    debug=True,
    routes=routes,
)
